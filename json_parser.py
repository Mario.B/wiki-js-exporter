import json
from page import Page
from typing import List


def parse_get_ids_response(response: str) -> List[int]:
    j = json.loads(response)
    result = []

    list = j["data"]["pages"]["list"]
    for item in list:
        result.append(item["id"])

    return result


def parse_get_page_response(response: str) -> Page:
    j = json.loads(response)

    jPage = j["data"]["pages"]["single"]
    return Page(jPage["path"], jPage["title"], jPage["content"])
