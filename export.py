from graph_ql_client import GraphQLClient
from wiki_js_exporter import Exporter

api_token = "your api token goes here"
url = "your GraphQl url goes here"

def main():
    exporter = Exporter(url, api_token)
    exporter.export()


if __name__ == "__main__":
    main()
